<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Sasaya Online E-Commerce store</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
       <!-- home page  main carousel -->
       <section class="primarycarousel">
            <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>                  
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active slider01">                        
                        <article class="p-3 position-absolute">
                            <h1 class="h2">This Festival <span>Celebrations with us</span></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                        </article>
                    </div>
                    <div class="carousel-item slider02">
                        
                        <article class="p-3 position-absolute">
                                <h2 class="h2"><span>This Festival </span> Celebrations with us</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                            </article>
                    </div>
                    <div class="carousel-item slider03">
                        
                        <article class="p-3 position-absolute">
                                <h2 class="h2"><span>This Festival Celebrations</span> with us</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                            </article>
                    </div>
                </div>                  
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>                  
            </div>
       </section>
       <!--/ home page main carousel-->

       <!-- top offers -->
       <section class="homeOffers py-4">
           <div class="container">
               <!-- title-->
               <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="indextitle">
                            <h2>Top OFFERS at sasaya</h2>
                            <p class="py-3">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
                </div>
                <!--title -->
                <!-- offers slider -->
                <div class="offers-slider">
                    <div id="demooffer" class="carousel slide" data-ride="carousel">                        
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-lg-6 align-self-center">
                                        <div class="offersimg position-relative">
                                            <img class="img-fluid" src="img/data/offer01.jpg" alt="Slider description">
                                            <div class="offerarticle text-center d-flex">
                                                <article class="align-self-center text-center">
                                                    <h4>Corporate Gift Articles</h4>
                                                    <h2>50%<span class="d-block">Off</span></h2>
                                                    <a href="javascript:void(0)">Shop Now</a>                                                        
                                                </article>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="videooffer" style="position:relative;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/51UbYJvEC14?ecver=2" style="position:absolute;width:100%;left:0" width="100%" height="100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                                    </div>
                                </div>  
                            </div>
                            <div class="carousel-item ">
                                <div class="row">
                                    <div class="col-lg-6">
                                            <div class="offersimg position-relative">
                                                <img class="img-fluid" src="img/data/offer02.jpg" alt="Slider description">
                                                <div class="offerarticle text-center d-flex">
                                                    <article class="align-self-center text-center">
                                                        <h4>Corporate Gift Articles</h4>
                                                        <h2>50%<span class="d-block">Off</span></h2>
                                                        <a href="javascript:void(0)">Shop Now</a>                                                        
                                                    </article>
                                                </div>
                                            </div>  
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="videooffer" style="position:relative;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/Ol5o04y_lQc?ecver=2" style="position:absolute;width:100%;left:0" width="100%" height="100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                                    </div>
                                </div>  
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="offersimg position-relative">
                                                <img class="img-fluid" src="img/data/offer03.jpg" alt="Slider description">
                                            <div class="offerarticle text-center d-flex">
                                                <article class="align-self-center text-center">
                                                    <h4>Corporate Gift Articles</h4>
                                                    <h2>50%<span class="d-block">Off</span></h2>
                                                    <a href="javascript:void(0)">Shop Now</a>                                                        
                                                </article>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="videooffer" style="position:relative;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/51UbYJvEC14?ecver=2" style="position:absolute;width:100%;left:0" width="100%" height="100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                                    </div>
                                </div>  
                            </div>
                        </div>                  
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demooffer" data-slide="prev">
                                <i class="fas fa-chevron-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#demooffer" data-slide="next">
                                <i class="fas fa-chevron-right"></i>
                        </a>                  
                    </div>
                </div>
                <!--/ offers slider -->
           </div>
       </section>
       <!--/ top offers -->

       <!--top categories tab-->
       <section class="categoriesHome py-5 homeOffers">
            <div class="container">
                <!-- title-->
               <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="indextitle">
                            <h2>Top Categories</h2>
                            <p class="py-3">Yes, this is our new collection, check it out our new arrivals.</p>
                        </article>
                    </div>
                </div>
                <!--/title -->
                <!--tab-->
                 <?php include 'homecategories.php' ?>
                <!--/tab-->
            </div>
       </section>
       <!--/ top categories tab-->
       <!-- banner advertisement -->
       <section class="banner-advertisement position-relative">
           <div class="container">
               <div class="row justify-content-center">
                   <div class="col-lg-6 text-center">
                       <article>
                            <h3 class="text-uppercase">NEW ARRIVAL GREEN TEA ACCESSORIES</h3>
                            <h2 class="py-2"><span>25% OFF</span> FOR ALL</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiunt ut labore et dmagna aliqua.</p>
                        </article>
                        <a href="productlist.php" >SHOP NOW</a>
                   </div>
               </div>
           </div>
       </section>
       <!--/ banner advertisement -->

       <!-- categories tiles -->
       <section class="categoryTiles my-5">
           <div class="container">
               <div class="row">
                   <div class="col-lg-8 ">
                       <div class="row">
                           <div class="col-lg-12">
                               <div class="tilecol orangebg">
                                    <figure class="position-relative">
                                        <img src="img/tilesindex01.jpg" class="img-fluid" title="" alt="">
                                        <article class="text-center position-absolute w-100">
                                            <h4>Gift Articles</h4>
                                            <p class="py-2"> Wooden Gift Articles, Handicrafts, Return Gifts, Customised Gifts, Corporate Gifts, etc...</p>
                                            <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                        </article>
                                    </figure>                                    
                                </div>
                           </div>
                       </div>
                       <div class="row aromarow">
                           <div class="col-lg-6">
                                <div class="tilecol aromasbg">
                                    <figure class="position-relative">
                                        <img src="img/tilesindex02.jpg" class="img-fluid" title="" alt="">
                                        <article class="text-center position-absolute w-100">
                                            <h4>Aroma's</h4>
                                            <p class="py-2">Aroma diffusers, Essential Oils, Accessories, Reed Diffusers</p>
                                            <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                        </article>
                                    </figure>                                    
                                </div> 
                           </div>
                           <div class="col-lg-6">
                                <div class="tilecol teasbg">
                                    <figure class="position-relative">
                                        <img src="img/tilesindex03.jpg" class="img-fluid" title="" alt="">
                                        <article class="text-center position-absolute w-100">
                                            <h4>Tea's</h4>
                                            <p class="py-2">Green Tea's, Tea Blend's, Tea Bags Accessories</p>
                                            <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                        </article>
                                    </figure>                                    
                                </div> 
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-4">
                        <div class="tilecol accbg">
                            <figure class="position-relative">
                                <img src="img/tilesindex04.jpg" class="img-fluid" title="" alt="">
                                <article class="text-center position-absolute w-100">
                                    <h4>Accessories</h4>
                                    <p class="py-2">Beauty Products, Brass Decoration Items, 
                                            Marriage Gift Sets, Household Gift Sets, 
                                            Electronic Items</p>
                                    <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                </article>
                            </figure>                                    
                        </div> 
                   </div>
               </div>
               <!--row-->
               <div class="row mt20">
                   <div class="col-lg-6">
                        <div class="tilecol plantsbg">
                            <figure class="position-relative">
                                <img src="img/tilesindex05.jpg" class="img-fluid" title="" alt="">
                                <article class="text-center position-absolute w-100">
                                    <h4>Plants</h4>
                                    <p class="py-2">Green Tea's, Tea Blend's, Tea Bags Accessories</p>
                                    <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                </article>
                            </figure>                                    
                        </div> 
                   </div>
                   <div class="col-lg-6">
                        <div class="tilecol saltsbg">
                            <figure class="position-relative">
                                <img src="img/tilesindex06.jpg" class="img-fluid" title="" alt="">
                                <article class="text-center position-absolute w-100">
                                    <h4>Salts</h4>
                                    <p class="py-2">Green Tea's, Tea Blend's, Tea Bags Accessories</p>
                                    <a href="productlist.php" class="text-uppercase my-2">VIEW ALL</a>
                                </article>
                            </figure>                                    
                        </div> 
                   </div>
               </div>
               <!--/ row-->
           </div>
       </section>
       <!--/ categoties tiles -->

       <!-- about sasaya-->
       <section class="aboutsasaya">
           <div class="container">
               <div class="row justify-content-center">
                   <div class="col-lg-12 text-center">
                       <article class="bgname">
                           <h2>SASAYA</h2>
                       </article>
                   </div>
               </div>
               <div class="row justify-content-center pt-3 pb-5">
                    <div class="col-lg-6 text-center contentin">
                        <h2 class="py-2">WELCOME TO SAYAS ONLINE STORE</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                        <a href="about.php" class="text-uppercase mt-2">Know More</a>
                    </div>
                </div>
           </div>
       </section>
       <!--/ about sasaya -->

       <!-- policies -->
       <div class="policies">
           <div class="container">
               <div class="row">
                   <div class="col-lg-4 text-center border-right">
                       <img src="img/rupee.svg" alt="" title="">
                       <h3>100% MONEY BACK GUARANTEE</h3>
                       <p>We offer a 100% money-back guarantee with in 60 days of payment</p>
                   </div>
                   <div class="col-lg-4 text-center border-right">
                        <img src="img/delivery-truck.svg" alt="" title="">
                        <h3>FREE SHIPPING DELIVERY</h3>
                        <p>All orders of Rs:2500 of eligible items across any product category qualify.</p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <img src="img/alarm-clock.svg" alt="" title="">
                        <h3>24/7 CUSTOMER SERVICES</h3>
                        <p>We have been working hard to better our overall customer experience</p>
                    </div>
               </div>
           </div>
       </div>
       <!--/ policies -->

    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>