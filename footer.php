<footer>
        <!-- top footer -->
        <div class="topfooter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">About Sasaya</h3>
                        <p class="pb-3">We possess within us two minds. So far I have written only of the scious mind.We further know that the subconscious has recorded every event. This is just perfect theme.</p>
                        <h3 class="fcoltitle text-uppercase">News Letter</h3>
                        <!-- news letter form -->
                        <div class="subscribe">
                            <form>
                                <input type="text" placeholder="Enter Your Email">
                                <input type="submit" value="Subscribe with us">
                            </form>
                        </div>
                        <!--/ news letter form -->
                    </div>
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Company</h3>
                        <ul>
                            <li><a href="about.php">About us</a></li>                           
                            <li><a href="blog.php">Blog</a></li>
                            <li><a href="faq.php">faq</a></li>
                            <li><a href="contact.php">Contact</a></li>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="returnpolicy.php">Return Policy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Categories</h3>
                        <ul>
                            <li><a href="productlist.php">Gift Articles</a></li>
                            <li><a href="productlist.php">Aroma’s</a></li>
                            <li><a href="productlist.php">Green Tea & Accessories</a></li>
                            <li><a href="productlist.php">Beauty Products</a></li>
                            <li><a href="productlist.php">Marriage Gift Sets</a></li>
                            <li><a href="productlist.php">House Hold Gift Sets</a></li>
                            <li><a href="productlist.php">Wooden Gift Articles</a></li>
                            <li><a href="productlist.php">Handicrafts</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <h3 class="fcoltitle text-uppercase">Get in Touch with us</h3>
                        <table width="100%" class="mb-3">
                            <tr>
                                <td><i class="fas fa-map-marker-alt"></i></td>
                                <td>
                                    <p>H.No: 10-1124, Plot No: 72, Road No:32, Banjarahills, Hyderabad, Telangana, India - 500035</p>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-phone-volume"></i></td>
                                <td>
                                    <p>+63 918 4084 694</p>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-envelope"></i></td>
                                <td>
                                    <p>info@sayas.com</p>
                                </td>
                            </tr>
                        </table>
                        <h3 class="fcoltitle text-uppercase">Follow us on</h3>
                        <ul class="nav footersocial">
                            <li class="nav-item"><a href="javascript:void(0)" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="nav-item"><a href="javascript:void(0)" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="nav-item"><a href="javascript:void(0)" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--/ top footer -->
        <!-- bottom footer-->
        <div class="bottomfooter position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <p class="pt-2">All Rights Reserved by Sayas e-Commerce Online store 2018</p>
                    </div>
                    <div class="col-lg-6 text-right">
                        <img src="img/paymenticon.png" alt="" title="">
                    </div>
                </div>
            </div>
            <a id="movetop" class="" href="javascript:void(0)"><i class="fas fa-arrow-up"></i></a>
        </div>
        <!--/ bottom footer -->
    </footer>

    