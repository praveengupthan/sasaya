<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Blog Detail Name will be here</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerblog.php'?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage pt-0">           
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row blogdetail">
                        <!-- blog left -->
                        <div class="col-lg-8">
                            <figure><img src="img/data/blog01.jpg" alt="" title="" class="img-fluid w-100"></figure>
                            <article class="">
                                <h4 class="py-3">Sasaya has given me the drive to do much more in life!</h4>
                                <p class="text-justify pb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>                            
                                <p class="text-justify pb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>
                                <p class="text-justify pb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </article>
                        </div>
                        <!--/ blog left -->
                        <!-- Recent Blog -->
                        <div class="col-lg-4">
                            <h4 class="pb-3">Recent News & Updates</h4>
                            <div class="recentblog">
                                <div class="row">
                                    <div class="col-lg-12 pb-2">
                                        <div class="recentin border-bottom pb-3">
                                            <figure class=""><a href="blogdetail.php"><img src="img/data/blog01.jpg" alt="" title="" class="img-fluid"></a></figure>
                                            <h6 class="h6"><a href="blogdetail.php">Mumpreneur’s journey from a monthly</a> </h5>
                                            <p class="text-justify">The story of every seller on Snapdeal is unique, but what is common is their entrepreneurial spirit, </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 pb-2">
                                        <div class="recentin border-bottom pb-3">
                                            <figure class=""><a href="blogdetail.php"><img src="img/data/blog02.jpg" alt="" title="" class="img-fluid"></a></figure>
                                            <h6 class="h6"><a href="blogdetail.php">Mumpreneur’s journey from a monthly</a> </h5>
                                            <p class="text-justify">The story of every seller on Snapdeal is unique, but what is common is their entrepreneurial spirit, </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 pb-2">
                                        <div class="recentin border-bottom pb-3">
                                            <figure class=""><a href="blogdetail.php"><img src="img/data/blog03.jpg" alt="" title="" class="img-fluid"></a></figure>
                                            <h6 class="h6"><a href="blogdetail.php">Mumpreneur’s journey from a monthly</a> </h5>
                                            <p class="text-justify">The story of every seller on Snapdeal is unique, but what is common is their entrepreneurial spirit, </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 pb-2">
                                        <div class="recentin border-bottom pb-3">
                                            <figure class=""><a href="blogdetail.php"><img src="img/data/blog04.jpg" alt="" title="" class="img-fluid"></a></figure>
                                            <h6 class="h6"><a href="blogdetail.php">Mumpreneur’s journey from a monthly</a> </h5>
                                            <p class="text-justify">The story of every seller on Snapdeal is unique, but what is common is their entrepreneurial spirit, </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Recent blog -->
                        
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page --> 

    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>