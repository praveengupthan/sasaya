<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">
        <li>Gift Articles</li>
        <li>Aroma's</li>
        <li>Tea's</li>
        <li>Accessories</li>
        <li>Salts</li>
        <li>Plants</li>
    </ul>
    <div class="resp-tabs-container hor_1">
        <!-- gift articles -->
        <div>
            <div class="row">
                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Gift Articles-->

        <!-- aromas -->
        <div>
            <div class="row">

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        <!--/ aromas -->

        <!-- teas -->
        <div>
            <div class="row">

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/teas08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        <!--/ teas -->
        <!-- accessories -->
        <div>
            <div class="row">

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        <!--/ accessories -->
        <!-- salts-->
        <div>
            <div class="row">
                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/salts08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div> 

            </div>
        </div>
        <!--/ salts-->
        <!-- plants -->
        <div>
            <div class="row">

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant01.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div> 

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant02.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant03.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant04.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant05.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant06.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant07.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="singleproduct">
                        <figure class="text-center position-relative">
                            <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant08.png" alt="" title=""></a>
                            <span class="discount position-absolute">-18%</span>
                            <!-- hover -->
                            <div class="prohover">
                                <ul>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                </ul>
                            </div>
                            <!--/ hover-->
                        </figure>
                        <article class="text-center py-2">
                            <a class="d-block text-center" href="productdetail.php">Product Name will be here</a>
                            <ul class="text-center">
                                <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                <li><i class="fas fa-rupee-sign"></i>110.00</li>
                            </ul>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        <!--/ plants -->
    </div>
</div>
<!--/tab-->