<!-- script files -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/easyResponsiveTabs.js"></script>
    <script src="js/jquery.basictable.min.js"></script>
    <script src="js/custom.js"></script>    
    <!-- [if lt IE 9]>
    <script src="js/html5-shiv.js"></script>
    <![endif]-->
    <script src="js/simplegallery.min.js"></script>
