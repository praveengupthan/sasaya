$(document).ready(function(){
//popover
$('[data-toggle="popover"]').popover({
    html: true,
    content: function() {
        return $('#popover-content').html();
    }
});
//tooltip
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
});


//top updated news hidden on click
$(".closenews").click(function(){
    $(".newsupdates").css("margin-top","-40px");
});
//top updated news hidden on click ends

//horizontal responsive tab and vertical tab scripts starts 
$(document).ready(function () {
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);

            $name.text($tab.text());

            $info.show();
        }
    });

    $('#ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });

    //Vertical Tab
    $('#parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

});
// horzontal responsive tab and vertical tab script ends

//on hover of product column add class
$('.singleproduct').hover(function(){
    $(this).toggleClass('hoverproduct');  
});

// onclick window move to top
$(window).scroll(function(){
    if ($(this).scrollTop() > 100){
        $('#movetop').fadeIn('fast');
    }else{
        $('#movetop').fadeOut('fast');
    }
});
$(document).on('click', '#movetop', function() {
    $("html").animate({
        scrollTop: 0
    }, 500);
    return false;
});

//header on scroll add box shadow in navigation
$(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
        $(".fixed-top").addClass("fixed-theme");
    } else {
        $(".fixed-top").removeClass("fixed-theme");
    }
});

//accordian 
// Hiding the panel content. If JS is inactive, content will be displayed
$( '.panel-content' ).hide();

// Preparing the DOM

// -- Update the markup of accordion container 
$( '.accordion' ).attr({
  role: 'tablist',
  multiselectable: 'true'
 });

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$( '.panel-content' ).attr( 'id', function( IDcount ) { 
  return 'panel-' + IDcount; 
});
$( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) { 
  return 'control-panel-' + IDcount; 
});
$( '.panel-content' ).attr( 'aria-hidden' , 'true' );
// ---- Only for accordion, add role tabpanel
$( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );

// -- Wrapping panel title content with a <a href="">
$( '.panel-title' ).each(function(i){
  
  // ---- Need to identify the target, easy it's the immediate brother
  $target = $(this).next( '.panel-content' )[0].id;
  
  // ---- Creating the link with aria and link it to the panel content
  $link = $( '<a>', {
    'href': '#' + $target,
    'aria-expanded': 'false',
    'aria-controls': $target,
    'id' : 'control-' + $target
  });
  
  // ---- Output the link
  $(this).wrapInner($link);  
  
});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$( '.panel-title a' ).append('<span class="icon">+</span>');

// Now we can play with it
$( '.panel-title a' ).click(function() {
  
  if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !
    
    // -- Only for accordion effect (2 options) : comment or uncomment the one you want
    
    // ---- Option 1 : close only opened panel in the same accordion
    //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
    $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

    // Option 2 : close all opened panels in all accordion container
    //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);
    
    // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
    $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

  } else { // The current panel is opened and we want to close it

    $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

  }
  // No Boing Boing
  return false;
});


//table responsive script

$('.tableresp').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-container-breakpoint').basictable({
        containerBreakpoint: 485
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
});

//product detail gallery thumbnail plugin
$(document).ready(function(){
    $('.gallery').simplegallery({
        galltime : 400,
        gallcontent: '.content',
        gallthumbnail: '.thumbnail',
        gallthumb: '.thumb'
    });

    //input file 
    document.querySelector("html").classList.add('js');
    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");
    button.addEventListener("keydown", function(event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener("click", function(event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener("change", function(event) {
        the_return.innerHTML = this.value;
    });

   

});

