<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Change Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Change Password </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Change Password</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">Change Password</h5>
                                <!-- account right body -->
                                <div class="rightprofile">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <form class="py-2">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label>Current Password</label>
                                                        <input type="password" placeholder="************" class="form-control"> <span class="showpw"><i class="fas fa-eye-slash"></i></span> </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label>New Password</label>
                                                        <input type="password" placeholder="************" class="form-control"> <span class="showpw"><i class="fas fa-eye-slash"></i></span> </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label>Confirm New Password</label>
                                                        <input type="password" placeholder="************" class="form-control"> <span class="showpw"><i class="fas fa-eye-slash"></i></span> </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="py-2 pl-5 passwordmust">
                                            <h6 class="h6 fmed">Your new password must</h6>
                                            <ul class="list-group custom-popover pt-3">
                                                <li class="list-group-item"><span>At least 6 Characters</span></li>
                                                <li class="list-group-item"><span>At least 1 Upper case letter (A - Z)</span></li>
                                                <li class="list-group-item"><span>At least 1 Lower case Letter (a - z)</span></li>
                                                <li class="list-group-item"><span>At least 1 Number (0 - 9)</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                        <div class="row py-3">
                                            <div class="col-lg-4 mx-auto">
                                                <div class="form-group mt-2"> 
                                                    <input type="submit" value="change Password" class="btn text-uppercase">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>