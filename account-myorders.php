<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Account My Orders</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">My Orders</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">My Orders</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">My Orders</h5>
                                <!-- account right body -->
                                <div class="rightprofile">
                                     <!--Horizontal Tab-->
                                     <div class="ordertab">
                                            <div id="parentHorizontalTab">
                                                <ul class="resp-tabs-list hor_1">
                                                    <li>All</li>
                                                    <li>Awaiting payment <span>(1) </span></li>
                                                    <li>Shipping <span>(0)</span> </li>
                                                    <li>Completed <span>(1)</span></li>
                                                </ul>
                                                <div class="resp-tabs-container hor_1">
                                                    <!-- all products -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <h4 class="ordtitle fmed">Waiting for payment</h4>
                                                            <ul class="orddetlist">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Santosh Chary</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="account-myordersdetail.php"><img src="img/data/acc03.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="javascript:void(0)" class="cbtn btn text-uppercase fgray">PAYNOW</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="s-product py-3">
                                                            <h4 class="ordtitle fmed">Delivered</h4>
                                                            <ul class="orddetlist">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Santosh Chary</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="account-myordersdetail.php"><img src="img/data/acc04.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ all products -->
                                                    <!-- awaiting payments -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <h4 class="ordtitle fmed">Waiting for payment</h4>
                                                            <ul class="orddetlist">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Praveen Kumar</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="account-myordersdetail.php"><img src="img/data/acc05.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgrey">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="checkout.php" class="cbtn btn text-uppercase fgray">PAYNOW</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ awaiting payments -->
                                                    <!-- shipping -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <h4 class="ordtitle fmed">Shipped Product</h4>
                                                            <ul class="orddetlist">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Santosh Chary</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="account-myordersdetail.php"><img src="img/data/acc06.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="checkout.php" class="cbtn btn text-uppercase fgray">PAYNOW</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ shipping -->
                                                    <!-- completed -->
                                                    <div>
                                                        <div class="s-product border-bottom py-3">
                                                            <h4 class="ordtitle fmed">Completed</h4>
                                                            <ul class="orddetlist">
                                                                <li> <span>Name:</span> <span class="fgray pl-2">Santosh Chary</span> </li>
                                                                <li> <span>Order number:</span> <span class="fgray pl-2">18100614451880850561</span> </li>
                                                                <li> <span>Order Date &amp; Time:</span> <span class="fgray pl-2">06 Oct 2018 14:45:19</span> </li>
                                                            </ul>
                                                            <div class="row py-2 ordrow">
                                                                <div class="col-lg-2">
                                                                    <figure class="cartimg">
                                                                        <a href="account-myordersdetail.php"><img src="img/data/acc07.png"> </a>
                                                                    </figure>
                                                                </div>
                                                                <div class="col-lg-6 align-self-center">
                                                                    <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                                    <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                                    <div class="paybtns pt-3"> <a href="checkout.php" class="cbtn btn text-uppercase fgray">PAYNOW</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                                                </div>
                                                                <div class="col-lg-4 align-self-right text-center">
                                                                    <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ completed -->
                                                </div>
                                            </div>
                                        </div>
                                        <!--/Horizontal Tab-->
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>