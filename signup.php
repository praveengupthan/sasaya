<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Register with Sasaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>    
    <!--main login and register -->
    <main>
        <section class="sign">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 leftsign"></div>
                    <div class="col-lg-6 align-self-center">
                        <div class="signin mx-auto">
                            <figure class="text-center signlogo"><a href="index.php"><img src="img/logo.png" alt="" title=""></a></figure>
                            <article class="text-center py-2">
                                <h3>Sign up</h3>
                                <h4 class="pt-3 pb-2">Register with Sasaya</h4>
                            </article>
                            <form>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control" type="text" placeholder="Enter your Email">
                                </div>
                                <div class="form-group">
                                    <label>Mobile Number</label>
                                    <input class="form-control" type="text" placeholder="Enter your Mobile Number">
                                </div>
                                <div class="form-group">
                                    <label>Create Password</label>
                                    <input class="form-control" type="text" placeholder="Create Password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" type="text" placeholder="Confirm Password">
                                </div>                                
                            </form>
                            <div class="text-center"><input onclick="window.location.href='account-profileinfo.php'" class="btn" type="button" value="Signup"></div>
                            <p class="text-center pt-4">Already have an Account? <span><a href="login.php">Signin</a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>      
    </main>
    <!--/ main login and register -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>