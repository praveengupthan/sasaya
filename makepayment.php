<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Make Payment</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Make a Payment <span>3 (Products) </span> </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="cart.php">Cart</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Makepayment</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container payment">  
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="paymentcol px-4 py-3 border-rounded">
                                <article class="pb-4 mb-4 border-bottom text-center">
                                    <img src="img/check.png" alt="">
                                    <h5 class="h5">Your Order has been successfully submitted !</h5>
                                    <p>Please complete the payment within 24 hours to avoid the order being cancelled automatically.</p>
                                </article>
                                <article>
                                    <h5 class="h5">Order Details</h5>
                                    <table class="table tableresp">
                                        <thead>
                                            <tr>
                                                <th>Order Number</th>
                                                <th>Address</th>
                                                <th class="pricepayment">Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>5181006520590354</td>
                                                <td>Praveen Guptha N, H.No: 10-2/1, Ganapathi Nivas, Chandra Nagar, Chintal, Jeedimetla, Hyderabad, Telangana</td>
                                                <td><h3 class="h3">Rs: 950;</h3></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </article>
                            </div>
                            <div class="paymentcol p-3 border-rounded mt-4">
                                <h5 class="h5">Choose Payment</h5>
                                <div id="parentVerticalTab" class="paymenttab">
                                    <ul class="resp-tabs-list hor_1">
                                        <li><span><i class="fas fa-wallet"></i></span>Wallet</li>
                                        <li><span><i class="fas fa-credit-card"></i></span>Cards</li>
                                        <li><span><i class="fas fa-university"></i></span>Net Banking</li>
                                        <li><span><i class="far fa-money-bill-alt"></i></span>Cash on Delivery</li>
                                    </ul>
                                    <div class="resp-tabs-container hor_1 ">
                                        <!-- wallet -->
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon01.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">PayTM</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon02.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Amazon Pay</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon03.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Mobikwik</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon04.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Freecharge</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / wallet -->
                                        <!--cards -->
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon01.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">PayTM</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon02.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Amazon Pay</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon04.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Freecharge</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon03.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Mobikwik</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <!--/ cards -->
                                        <!-- net banking -->
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon01.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">PayTM</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon02.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Amazon Pay</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon03.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Mobikwik</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon04.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Freecharge</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ net banking -->

                                        <!--cash on delivery -->
                                        <div>
                                            <div class="row">                                                
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon02.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Amazon Pay</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon01.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">PayTM</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon03.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Mobikwik</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="paymentcol">
                                                        <div class="row">
                                                            <div class="col-lg-6 align-self-center text-center">
                                                                <a href="javascript:void(0)"><img src="img/paymentmethodicon04.png" alt="" title=""></a>
                                                            </div>
                                                            <div class="col-lg-6 align-self-center">
                                                                <a href="javascript:void(0)">
                                                                    <p class="fmed py-1">Freecharge</p>
                                                                    <p class="text-uppercase fred fmed">Link Account</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ cashon delivery -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>