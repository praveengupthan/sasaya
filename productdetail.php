<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Product Detail</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Product Name will be here </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">Category Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Product Name will be here</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row">               
                        <!-- product gallery -->
                        <div class="col-lg-6">
                        <section class="simplegallery gallery">
                            <div class="content">
                                <img src="img/data/acc02.png" class="image_1" alt="" />
                                <img src="img/data/acc03.png" class="image_2" style="display:none" alt="" />
                                <img src="img/data/acc04.png" class="image_3" style="display:none" alt="" />
                                <img src="img/data/acc05.png" class="image_4" style="display:none" alt="" />
                                <img src="img/data/acc06.png" class="image_5" style="display:none" alt="" />
                                <img src="img/data/acc07.png" class="image_6" style="display:none" alt="" />
                            </div>          

                            <div class="thumbnail">
                                <div class="thumb">
                                    <a href="#" rel="1">
                                        <img src="img/data/acc02.png" id="thumb_1" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="2">
                                        <img src="img/data/acc03.png" id="thumb_2" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="3">
                                        <img src="img/data/acc04.png" id="thumb_3" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="4">
                                        <img src="img/data/acc05.png" id="thumb_4" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="5">
                                        <img src="img/data/acc06.png" id="thumb_5" alt="" />
                                    </a>
                                </div>
                                <div class="thumb last">
                                    <a href="#" rel="6">
                                        <img src="img/data/acc07.png" id="thumb_6" alt="" />
                                    </a>
                                </div>
                            </div>
                        </section>   
                        </div>
                        <!--/ product gallery -->
                        <!-- product basic information -->
                        <div class="col-lg-6">
                            <div class="rtproduct">
                                <h3 class="h3">New Painted Couple Peacock Crafts</h3>
                                <p class="price py-2 border-bottom">
                                    <span class="oldprice">Rs: 7,600</span>
                                    <span class="ocolor">Rs: 6,600</span>
                                    <span class="sm">32% off</span>
                                </p>
                                <table class="rttable">
                                    <tr>
                                        <td>Availability</td>
                                        <td>:</td>
                                        <td>In Stock</td>
                                    </tr>
                                    <tr>
                                        <td>Product Type</td>
                                        <td>:</td>
                                        <td>Speaker</td>
                                    </tr>
                                    <tr>
                                        <td>Colour </td>
                                        <td>:</td>
                                        <td>Black</td>
                                    </tr>
                                    <tr>
                                        <td>Category </td>
                                        <td>:</td>
                                        <td>Gift Articles </td>
                                    </tr>
                                </table>                                
                                <ul class="nav">
                                    <li class="nav-item">
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                        </select>
                                    </li>
                                    <li class="nav-item"><a href="cart.php" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                    <li class="nav-item"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i> Add to Wishlist </a></li>
                                    <li class="nav-item"><a href="cart.php" data-toggle="tooltip" data-placement="bottom" title="Buy Now"><i class="fas fa-rupee-sign"></i> Buy Now</a></li>
                                </ul>
                               
                                <!-- share this product -->
                                <p class="pt-3">Share this product</p>
                                <ul class="sharepro nav pt-2">
                                    <li class="nav-item"> <a class="fb" href="javascript:void(0)" target="_blank"><i class="fab fa-facebook-f"></i> Share <span>0</span></a> </li>
                                    <li class="nav-item"> <a class="tw" href="javascript:void(0)" target="_blank"><i class="fab fa-twitter"></i> Tweet <span>0</span></a> </li>
                                    <li class="nav-item"> <a class="pin" href="javascript:void(0)" target="_blank"><i class="fab fa-pinterest-p"></i> Pin it <span>0</span></a> </li>
                                    <li class="nav-item"> <a class="gplus" href="javascript:void(0)" target="_blank"><i class="fab fa-google-plus-g"></i> <span>+1</span></a> </li>
                                </ul>
                                <!--/ share this product -->
                                
                            </div>
                        </div>
                        <!--/ product basic information -->                       
                    </div> 

                     <!--row tab-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="productdetailtab pt-4">
                                <!--tab-->
                             <div id="parentHorizontalTab">
                                <ul class="resp-tabs-list hor_1">
                                    <li>Overview</li>
                                    <li>Specifications</li> 
                                    <li>Review</li>                                                                                        
                                </ul>
                                <div class="resp-tabs-container hor_1">
                                    <!-- Overview-->
                                    <div>
                                      <div class="row">
                                          <div class="col-lg-4">
                                              <img src="img/overviewimg.jpg" class="img-fluid">
                                          </div>
                                          <div class="col-lg-8 align-self-center">
                                              <h4 class="h4">Product Overview</h4>
                                              <p class="text-justify pb-2">Introducing the innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less design, this NCFilmTM heater can be easily cleaned thus supports a long lifespan. The CUBIS Max atomizer allows a simple replacement of cotton for a pure and delicate vaping experience. The ULTEX T80, a powerful vape pen style system is beautifully crafted. It’s powered by single replaceable 18650 battery paired with multiple output modes. A 0.49-inch OLED screen is place on the downside. This intuitive display presents you with everything you need. Having the top-fill and top-airflow system, you’re free of any leakage. Best at 40w, you can enjoy a huge but flavored clouds.</p>
                                              <p class="text-justify">Introducing the innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less design, this NCFilmTM heater can be easily cleaned thus supports a long lifespan. The CUBIS Max atomizer allows a simple replacement of cotton for a pure and delicate.</p>

                                          </div>
                                      </div>
                                    </div>
                                    <!--/ Overview-->
                                    <!-- Specifications-->
                                    <div>
                                        <div class="specsdetails">
                                            <h5 class="h5 py-2">Parameter</h5>
                                            <table width="100%">
                                                <tr>
                                                    <td>Size</td>
                                                    <td>:</td>
                                                    <td>28.0*138.5mm </td>
                                                </tr>
                                                <tr>
                                                    <td>Weight</td>
                                                    <td>:</td>
                                                    <td>163.0g (no cell) </td>
                                                </tr>
                                                <tr>
                                                    <td>Colors</td>
                                                    <td>:</td>
                                                    <td>Matte Black, Silver, Gold, Sky Blue, Dazzling </td>
                                                </tr>
                                                <tr>
                                                    <td>Battery using</td>
                                                    <td>:</td>
                                                    <td>single high rate 18650 cell (CDR ≥ 25A) </td>
                                                </tr>
                                                <tr>
                                                    <td>E-liquid capacity</td>
                                                    <td>:</td>
                                                    <td>5.0ml </td>
                                                </tr>
                                                <tr>
                                                    <td>Heater</td>
                                                    <td>:</td>
                                                    <td>Joyetech NCFilmTM heater (Kanthal) </td>
                                                </tr>
                                                <tr>
                                                    <td>Screen</td>
                                                    <td>:</td>
                                                    <td>0.49 inch OLED display </td>
                                                </tr>
                                                <tr>
                                                    <td>Output</td>
                                                    <td>:</td>
                                                    <td>1-80W </td>
                                                </tr>
                                                <tr>
                                                    <td>Modes</td>
                                                    <td>:</td>
                                                    <td>POWER/BYPASS/TEMP(NI/TI/SS)/TCR </td>
                                                </tr>
                                                <tr>
                                                    <td>Resistance range </td>
                                                    <td>:</td>
                                                    <td>0.05-1.5ohm for TEMP/TCR mode 0.05-3.5ohm for POWER mode </td>
                                                </tr>
                                            </table>
                                            <h5 class="h5 py-2">Features</h5>
                                            <table width="100%">
                                                <tr>
                                                    <td>Coil-less design, innovative NCFilmTM heater</td>
                                                </tr>
                                                <tr>
                                                    <td>Huge clouds and optimal flavor</td>
                                                </tr>
                                                <tr>
                                                    <td>Long life span</td>
                                                </tr>
                                                <tr>
                                                    <td>Finely crafted, an integral whole</td>
                                                </tr>
                                                <tr>
                                                    <td>Powerful as mech mods</td>
                                                </tr>
                                                <tr>
                                                    <td>ntuitive OLED screen</td>
                                                </tr>
                                            </table>
                                            <h5 class="h5 py-2">Standard Configuration</h5>
                                            <table width="100%">
                                                <tr>
                                                    <td>Standard Configuration: </td>
                                                </tr>
                                                <tr>
                                                    <td>1 * ULTEX T80 (no cell) </td>
                                                </tr>
                                                <tr>
                                                    <td>1 * CUBIS Max atomizer (including 1 * NCFilmTM heater) </td>
                                                </tr>
                                                <tr>
                                                    <td>1 * QC USB cable </td>
                                                </tr>
                                                <tr>
                                                    <td>2 * Manual </td>
                                                </tr>
                                                <tr>
                                                    <td>1 * Warranty card </td>
                                                </tr>
                                                <tr>
                                                    <td>2 * Warning card Spare parts</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!--/Specifications-->  
                                    <!-- Rating-->
                                    <div>
                                        <div class="specsdetails">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h5 class="h5 py-2">Customer Reviews</h5>
                                                    <h6 class="h6"><span class="fred">19687 </span>Reviews</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 ratecol align-self-center">
                                                    <h2 class="fbold fred">4.9</h2>
                                                    <p class="ratestars"> <span><i class="fas fa-star fred"></i></span> <span><i class="fas fa-star fred"></i></span><span><i class="fas fa-star fred"></i></span><span><i class="fas fa-star fred"></i></span><span><i class="fas fa-star-half-alt fred"></i></span> </p>
                                                    <p class="ratep">4.9 out of 5 stars</p>
                                                </div>
                                                <!-- rate percentage -->
                                                <div class="col-lg-5">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="50px"><span>18407</span></td>
                                                            <td>
                                                                <div class="progress" style="height:10px">
                                                                    <div class="progress-bar" style="width:80%;height:10px"></div>
                                                                </div>
                                                            </td>
                                                            <td width="80px"> <span class="fmed starq"><input type="checkbox"> 5 Star</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px"><span>1182</span></td>
                                                            <td>
                                                                <div class="progress" style="height:10px">
                                                                    <div class="progress-bar" style="width:60%;height:10px"></div>
                                                                </div>
                                                            </td>
                                                            <td width="80px"> <span class="fmed starq"><input type="checkbox"> 4 Star</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px"><span>98</span></td>
                                                            <td>
                                                                <div class="progress" style="height:10px">
                                                                    <div class="progress-bar" style="width:20%;height:10px"></div>
                                                                </div>
                                                            </td>
                                                            <td width="80px"> <span class="fmed starq"><input type="checkbox"> 3 Star</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px"><span>0</span></td>
                                                            <td>
                                                                <div class="progress" style="height:10px">
                                                                    <div class="progress-bar" style="width:2%;height:10px"></div>
                                                                </div>
                                                            </td>
                                                            <td width="80px"> <span class="fmed starq"><input type="checkbox"> 2 Star</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px"><span>0</span></td>
                                                            <td>
                                                                <div class="progress" style="height:10px">
                                                                    <div class="progress-bar" style="width:1%;height:10px"></div>
                                                                </div>
                                                            </td>
                                                            <td width="80px"> <span class="fmed starq"><input type="checkbox"> 1 Star</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <!--/ rate percentage -->
                                                <!-- image -->
                                                <div class="col-lg-4 text-center align-self-center revdiv">
                                                    <figure class="revimg mx-auto"> 
                                                        <img src="img/data/acc01.png" alt="" title="" class="img-fluid"> 
                                                    </figure> 
                                                    <a class="btn btn2" href="javascript:void(0)">VIEW PRODUCT</a>
                                                </div>
                                                <!--/ image -->                                                
                                            </div>

                                             <!-- help ful -->
                                             <div class="helpfulreview">
                                                <h5 class="h5 py-1">Most Helpful</h5>

                                                <!-- row review -->
                                                <div class="row reviewrow border-bottom">
                                                    <div class="col-lg-1">
                                                        <figure>
                                                            <img src="img/data/gift06.png">
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <h4 class="h4">Product Name will be here</h4>
                                                        <p class="pb-3">India Standard Charger (2A Fast Charging</p>

                                                        <h6 class="h6">Product is Very Nice and Good for experience</h6>
                                                        <ul class="nav navhelpimg">
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                        </ul>
                                                        <!-- delivery to -->
                                                        <div class="deliverto pt-2">
                                                            <p class="fmed text-right"> <i class="far fa-thumbs-up"></i> Likes(131)</p>
                                                            <div class="inputdelivery py-2">
                                                                <input class="float-left" type="text" placeholder="Write down your reply" />
                                                                <input class="float-left" value="Reply" type="submit" /> 
                                                            </div>
                                                        </div>
                                                        <!-- /delivery to -->

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p>February 3, 2018</p>
                                                    </div>
                                                </div>
                                                 <!-- /row review -->

                                                 <!-- row review -->
                                                <div class="row reviewrow border-bottom">
                                                    <div class="col-lg-1">
                                                        <figure>
                                                            <img src="img/data/gift06.png">
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <h4 class="h4">Product Name will be here</h4>
                                                        <p class="pb-3">India Standard Charger (2A Fast Charging</p>

                                                        <h6 class="h6">Product is Very Nice and Good for experience</h6>
                                                        <ul class="nav navhelpimg">
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                            <li class="nav-item">
                                                                <img src="img/data/salts06.png">
                                                            </li>
                                                        </ul>
                                                        <!-- delivery to -->
                                                        <div class="deliverto pt-2">
                                                            <p class="fmed text-right"> <i class="far fa-thumbs-up"></i> Likes(131)</p>
                                                            <div class="inputdelivery py-2">
                                                                <input class="float-left" type="text" placeholder="Write down your reply" />
                                                                <input class="float-left" value="Reply" type="submit" /> 
                                                            </div>
                                                        </div>
                                                        <!-- /delivery to -->

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p>February 3, 2018</p>
                                                    </div>
                                                </div>
                                                 <!-- /row review -->

                                            </div>
                                            <!--/ help ful review -->                                      
                                        </div>                                  

                                    </div>
                                    <!--/Rating-->                                                                        
                                </div>
                             </div>
                             <!--/ tab-->
                            </div>
                        </div>
                    </div>
                    <!--/row tab-->

                    <!-- related products -->
                    <div class="relatedproducts">
                        <div class="row justify-content-center pb-4">
                            <div class="col-lg-6 text-center position-relative">
                                <h5 class="h5">Related Products</h5>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant01.png" alt="" title=""></a>
                                        <span class="discount position-absolute">-18%</span>
                                        <!-- hover -->
                                        <div class="prohover">
                                            <ul>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                            <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant02.png" alt="" title=""></a>
                                        <span class="discount position-absolute">-18%</span>
                                        <!-- hover -->
                                        <div class="prohover">
                                            <ul>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                            <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant03.png" alt="" title=""></a>
                                        <span class="discount position-absolute">-18%</span>
                                        <!-- hover -->
                                        <div class="prohover">
                                            <ul>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                            <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant04.png" alt="" title=""></a>
                                        <span class="discount position-absolute">-18%</span>
                                        <!-- hover -->
                                        <div class="prohover">
                                            <ul>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                            <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                    <!--/ related products -->
                </div>
               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>