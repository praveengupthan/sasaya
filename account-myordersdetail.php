<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Account My Orders</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">My Orders</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="account-profileinfo.php">User Name will be here</a></li>  
                                <li class="nav-item"><a class="nav-link" href="account-myorders.php">My Orders</a></li>                                                            
                                <li class="nav-item"><a class="nav-link">My Order Detail Name</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <table class="table no-border">
                                    <tr class="border-bottom">
                                        <td><h5 class="h5 fmed pb-3">Order Number 5180328108085519</h5></td>
                                        <td align="right"> <a href="account-myorders.php">Back to My Orders</a></td>
                                    </tr>
                                </table>
                                <!-- account right body -->
                                <div class="rightprofile ">                                    
                                        <h4 class="ordtitle fgreen fmed py-3">Delivered</h4>
                                        <div class="row py-2 ordrow ordertab">
                                            <div class="col-lg-2">
                                                <figure class="cartimg">
                                                    <a href="javascript:void(0)"><img src="img/data/acc03.png"> </a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-6 align-self-center">
                                                <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                                <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                            </div>
                                            <div class="col-lg-4 align-self-right text-center">
                                                <h6 class="price h6"><i class="fas fa-rupee-sign"></i> 498 X 1</h6>
                                            </div>
                                        </div>
                                        <!-- order status -->
                                        <div class="ord-status d-flex border-bottom dashedbrd">
                                            <div class="barcol"> <span class="fgreen sttext">Ordered</span>
                                                <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><i class="fas fa-circle"></i></a> </div>
                                            </div>
                                            <div class="barcol"> <span class="fgreen sttext">Packed</span>
                                                <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><i class="fas fa-circle"></i></a> </div>
                                            </div>
                                            <div class="barcol"> <span class="fgreen sttext">Shipped</span> <span class="fgreen sttext deliveredst">Delivered</span>
                                                <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><i class="fas fa-circle"></i></a> <a href="javascript:void(0)" class="delivered" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><i class="fas fa-circle"></i></a> </div>
                                            </div>
                                        </div>
                                        <!--/ order status -->
                                        <!--delivery details -->
                                        <div class="deliverydet py-2 border-bottom dashedbrd">
                                            <h4 class="ordtitle fmed py-3">Delivery Information</h4>
                                            <table width="100%" class="table-orderdetails">
                                                <tr>
                                                    <td width="30%">Tracking number </td>
                                                    <td><span class="fgray">69592503783</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Courier company </td>
                                                    <td><span class="fgray">Bluedart</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Delivery Date </td>
                                                    <td><span class="fgray">Delivered on Thu, Mar 8th '18
                                                        </span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="deliverydet py-2 border-bottom dashedbrd">
                                            <h4 class="ordtitle fmed py-3">Delivery Address</h4>
                                            <table width="100%" class="table-orderdetails">
                                                <tr>
                                                    <td width="30%">Name </td>
                                                    <td><span class="fgray">Santosh Chary</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Address </td>
                                                    <td><span class="fgray">2-3-64/10/A/23, Jaiswal Garden, Amberpet. Little Flower High School 500013 Hyderabad, Telangana</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">PHone </td>
                                                    <td><span class="fgray">9849367037</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="deliverydet py-2 border-bottom dashedbrd">
                                            <h4 class="ordtitle fmed py-3">Payment Mode and Delivery Time</h4>
                                            <table width="100%" class="table-orderdetails">
                                                <tr>
                                                    <td width="30%">Payment mode </td>
                                                    <td><span class="fgray">Credit Card</span></td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Delivery time </td>
                                                    <td><span class="fgray">Packages are delivered between 09:00 -19:00 from Monday to Saturday. There are no deliveries on Sunday and on public holidays.</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="row py-3">
                                            <div class="col-lg-4 text-right ml-auto text-right">
                                                <table class="finalprice" width="100%;">
                                                    <tr>
                                                        <td>Net Amount</td>
                                                        <td>:</td>
                                                        <td><span><i class="fas fa-rupee-sign"></i>498</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipping</td>
                                                        <td>:</td>
                                                        <td><span><i class="fas fa-rupee-sign"></i>0</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total amount</td>
                                                        <td>:</td>
                                                        <td><span><i class="fas fa-rupee-sign"></i>498</span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <!--/ delivelry details -->
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>