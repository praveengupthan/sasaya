<div class="order-summary">
    <h5 class="pb-3">Order Summary (2 Products)</h5>
    <table class="table-borderless">
        <tr>
            <td colspan="2"><strong>Palak Brown Flats</strong></td>                                        
        </tr>
        <tr class="border-bottom">
            <td>Qty: 01</td>                                        
            <td align="right">Rs:399</td>
        </tr>
            <tr>
            <td colspan="2"><strong>Tashveen Articles Ceramic Table Vase 22 cms </strong></td>                                        
        </tr>
        <tr class="border-bottom">
            <td>Qty: 01</td>                                        
            <td align="right">Rs:399</td>
        </tr>
            <tr>
            <td>Total</td>                                        
            <td align="right">Rs:899</td>
        </tr>
        <tr class="border-bottom">
            <td>Delivery Charges</td>                                        
            <td align="right">+ Rs:50</td>
        </tr>
        <tr class="border-bottom">
            <td>You pay</td>                                        
            <td align="right"><strong class="finalprice">+ Rs:956</strong></td>
        </tr>
    </table>
</div>