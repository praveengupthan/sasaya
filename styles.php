<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<!-- styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/easy-responsive-tabs.css">
    <link rel="stylesheet" href="scss/style.css">
    <link rel="stylesheet" href="css/accordian.css">
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <link rel="stylesheet" href="css/styleother.css">
    <link rel="stylesheet" href="css/simplegallery.demo1.css">
