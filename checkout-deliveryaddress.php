<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Checkout</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Checkout <span>3 (Products) </span> </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="cart.php">Cart</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Delivery Address</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row checkout">               
                        <!-- checkout left -->
                        <div class="col-lg-9">
                            <div class="resp-tabs-container hor_1">
                                <h5 class="h5 p-3 mb-4 checkoutheading">Delivery Address</h5>
                                <!-- Delivery address-->                                   
                                <div class="row">
                                    <!-- address form -->
                                    <div class="col-lg-7">
                                        <form>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Pincode:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Full Name</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <textarea class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Landmark</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" placeholder="Ex: Near Hockey Stadium">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>City</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select class="form-control w-100">
                                                            <option>Select City</option>
                                                            <option>Hyderabad</option>
                                                            <option>Secunderabad</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>State</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select class="form-control w-100">
                                                            <option>Select State</option>
                                                            <option>Telangana</option>
                                                            <option>Andhra Pradesh</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Mobile Number</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>                                                    

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>Address Type</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select class="form-control w-100">
                                                            <option>Select Address Type</option>
                                                            <option>Home</option>
                                                            <option>Office Pradesh</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <label><input class="mr-10" type="checkbox"><span class="pl-1">Make this Default Address</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="submit" value="SAVE" class="btn text-uppercase">
                                                        <input onclick="window.location.href='checkoutrevieworder.php'" type="submit" value="Review Order" class="btn text-uppercase">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <!--/ address form -->
                                    <!-- saved address-->
                                    <div class="col-lg-5">
                                        <div class="savedaddress">
                                            <h4>Saved Address</h4>
                                            <address class="">
                                                <h5>Name will be here</h5>
                                                <p>10-2/A, Beside Rajiv Gruha, Pragathi Nagar, Nizampet, ... Hyderabad - 500090, Telangana</p>
                                                <p>+91 96421235254</p>
                                                <span class="addresslabel home">Home</span>
                                                <p class="editdel">
                                                    <span><a href="javascript:void(0)"><i class="far fa-edit"></i> Edit</a></span>
                                                    <span><a href="javascript:void(0)"><i class="far fa-trash-alt"></i> Delete</a></span> 
                                                </p>
                                                <input type="submit" value="Deliver Here" class="btn">
                                            </address>

                                            <address class="">
                                                <h5>Name will be here</h5>
                                                <p>10-2/A, Beside Rajiv Gruha, Pragathi Nagar, Nizampet, ... Hyderabad - 500090, Telangana</p>
                                                <p>+91 96421235254</p>
                                                <span class="addresslabel office">Office</span>
                                                <p class="editdel">
                                                    <span><a href="javascript:void(0)"><i class="far fa-edit"></i> Edit</a></span>
                                                    <span><a href="javascript:void(0)"><i class="far fa-trash-alt"></i> Delete</a></span> 
                                                </p>
                                                <input type="submit" value="Deliver Here" class="btn">
                                            </address>

                                            <address class="">
                                                <h5>Name will be here</h5>
                                                <p>10-2/A, Beside Rajiv Gruha, Pragathi Nagar, Nizampet, ... Hyderabad - 500090, Telangana</p>
                                                <p>+91 96421235254</p>
                                                <span class="addresslabel others">Others</span>
                                                <p class="editdel">
                                                    <span><a href="javascript:void(0)"><i class="far fa-edit"></i> Edit</a></span>
                                                    <span><a href="javascript:void(0)"><i class="far fa-trash-alt"></i> Delete</a></span> 
                                                </p>
                                                <input type="submit" value="Deliver Here" class="btn">
                                            </address>
                                        </div>
                                    </div>
                                    <!--/ saved address -->
                                </div>                                    
                                <!--/ delivery address-->                                                                  
                            </div>
                        </div>
                        <!--/ checkout left -->
                        <!-- Order summary -->
                        <div class="col-lg-3">
                            <?php include 'checkoutsummary.php' ?>
                        </div>
                        <!--/ order summary -->
                    </div> 
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>