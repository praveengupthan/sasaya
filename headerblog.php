<header class=""> 
    <!-- nav bar -->
    <div class="container blognav">
        <nav id="topNav" class="navbar-expand-lg  pt-5">
            
            <div class="blogbrand text-center">
                <a  href="index.php"><img src="img/logo.png" alt="sasaya" title="Sasaya Online e Commerce Store" ></a>
            </div>
            
            <div id="navbardropmain" class="navbar-collapse collapse justify-content-center">
                <ul class="navbar-nav">
                    <li class="nav-item navactive">
                        <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Gift Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Aroma's</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Tea's</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Accessories</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Salts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php"> Plants</a>
                    </li>         
                </ul>            
            </div>
        </nav>
    </div>
    <!--/nav bar -->
</header>