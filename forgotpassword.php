<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Forgot Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>    
    <!--main login and register -->
    <main>
        <section class="sign">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 leftsign"></div>
                    <div class="col-lg-6 align-self-center">
                        <div class="signin mx-auto">
                            <figure class="text-center signlogo"><img src="img/logo.png" alt="" title=""></figure>
                            <article class="text-center py-2">
                                <h3>Forgot password?</h3>
                                <h4 class="pt-3 pb-2">Enter your details below.</h4>
                            </article>
                            <form>
                                <div class="form-group">
                                    <label>Enter your Registered Email Address</label>
                                    <input class="form-control" type="text" placeholder="Enter Your Registered Email">
                                </div>
                                <div class="form-group">
                                    <p class="text-center">You get Recet password link to your Registered Email Once you you will click on below link</p>
                                </div>                              
                                
                            </form>
                            <div class="text-center"><input class="btn" type="button" value="RESET PASSWORD"></div>
                            <p class="text-center pt-4">Back to <span><a href="login.php">Signin?</a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>      
    </main>
    <!--/ main login and register -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>