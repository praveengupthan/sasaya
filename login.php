<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Sasaya Online E-Commerce store</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>    
    <!--main login and register -->
    <main>
        <section class="sign">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 leftsign"></div>
                    <div class="col-lg-6 align-self-center">
                        <div class="signin mx-auto">
                            <figure class="text-center signlogo"><a href="index.php"><img src="img/logo.png" alt="" title=""></a></figure>
                            <article class="text-center py-2">
                                <h3>Sign in</h3>
                                <h4 class="pt-3 pb-2">Enter your details below.</h4>
                            </article>
                            <form>
                                <div class="form-group">
                                    <label>Enter User Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Your User name">
                                </div>
                                <div class="form-group">
                                    <label>Enter Password</label>
                                    <input class="form-control" type="password" placeholder="Enter Password">
                                </div>
                                <div class="form-group text-right">
                                    <a href="forgotpassword.php">Forgot Password?</a>
                                </div>
                            </form>
                            <div class="text-center"><input onclick="window.location.href='account-profileinfo.php'" class="btn" type="submit" value="LOGIN"></div>
                            <p class="text-center pt-4">Don't have an account? <span><a href="signup.php">Signup</a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>      
    </main>
    <!--/ main login and register -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>