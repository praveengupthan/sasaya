<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>About Sasaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">About us </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>                                
                                <li class="nav-item"><a class="nav-link">About Sasaya</a></li>
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row justify-content-center">  
                       <div class="col-lg-8 text-center"><img src="img/aboutimg01.png" class="img-fluid"></div>
                    </div> 
                    <div class="row justify-content-center pt-3">  
                       <div class="col-lg-10 text-center">
                            <h4 class="subtitle">WELCOME TO SASAYA</h4>
                            <p class="py-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                    </div>
                    <div class="row py-4">
                        <div class="col-lg-6 align-self-center">
                            <h5 class="medtitle"> Support Small Business </h5> 
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, </p> 
                        </div>
                        <div class="col-lg-6">
                            <img src="img/aboutimg02.png" alt="" title="" class="img-fluid">
                        </div>
                    </div>

                    <div class="row py-4">
                        <div class="col-lg-6 align-self-center order-lg-last order-sm-first">
                            <h5 class="medtitle"> Business Title will be here </h5> 
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, </p> 
                        </div>
                        <div class="col-lg-6">
                            <img src="img/aboutimg03.png" alt="" title="" class="img-fluid">
                        </div>
                    </div>


                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->   

    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>