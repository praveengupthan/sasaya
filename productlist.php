<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Gift Articles of Sasaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Gift Articles <span>(192 Items)</span></h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Category</a></li>
                                <li class="nav-item"><a class="nav-link">Gift Articles</a></li>
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row filterrow pb-4">
                        <div class="col-lg-4">
                            <span><i class="fas fa-search"></i></span><input type="text" placeholder="Search with keyword">
                        </div>
                        <div class="col-lg-8 text-right">
                            <!-- filters -->
                                <ul class="nav filterproduct float-right">
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Popular</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Price low to high</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Price High to Low</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Fresh Arrivals </a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Offers Products</a></li>
                                </ul>
                            <!--/ filters -->
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row products -->
                    <div class="row">

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant01.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant02.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant03.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/plant04.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc01.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc02.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc03.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/acc04.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas01.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas02.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/aromas03.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/tilesindex03.jpg" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift01.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift02.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift03.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-md-4">
                            <div class="singleproduct">
                                <figure class="text-center position-relative">
                                    <a href="javascript:void(0)"><img class="img-fluid" src="img/data/gift04.png" alt="" title=""></a>
                                    <span class="discount position-absolute">-18%</span>
                                    <!-- hover -->
                                    <div class="prohover">
                                        <ul>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Cart"><i class="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><i class="fas fa-external-link-alt"></i></a></li>
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wish list"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <!--/ hover-->
                                </figure>
                                <article class="text-center py-2">
                                    <a class="d-block text-center" href="javascript:void(0)">Product Name will be here</a>
                                    <ul class="text-center">
                                        <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>125.00</span></li>
                                        <li><i class="fas fa-rupee-sign"></i>110.00</li>
                                    </ul>
                                </article>
                            </div>
                        </div> 

                    </div>
                    <!--/ row products -->
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->   

    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>