<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Cart</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Your Cart </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>                                                          
                                <li class="nav-item"><a class="nav-link">Cart</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row cart">               
                        <!-- cart items -->
                        <div class="col-lg-12">
                            <table class="table tableresp">
                                <thead>
                                    <tr>
                                        <th>Item Details</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Delivery Details</th>
                                        <th>Sub Total</th>                               
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="cartfigsection">
                                                <figure class="cartfig float-left">
                                                    <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                </figure>
                                                <article>
                                                    <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                    <p>Orchid embroidery home furnishing articles... </p>
                                                    <p class="pt-2">
                                                        <a href="javascript:void(0)"><i class="fas fa-times"></i> Remove</a>
                                                        <a href="javascript:void(0)"><i class="far fa-heart"></i> Add to Wishlist</a>
                                                    </p>
                                                </article>
                                            </div>
                                        </td>
                                        <td>Rs: 1,600</td>
                                        <td>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </td>
                                        <td>
                                            <p>Standard Delivery By </p>
                                            <p>06 Dec - 10 Dec</p>
                                        </td>
                                        <td>RS: 420</td>                                
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="cartfigsection">
                                                <figure class="cartfig float-left">
                                                    <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                </figure>
                                                <article>
                                                    <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                    <p>Orchid embroidery home furnishing articles... </p>
                                                    <p class="pt-2">
                                                        <a href="javascript:void(0)"><i class="fas fa-times"></i> Remove</a>
                                                        <a href="javascript:void(0)"><i class="far fa-heart"></i> Add to Wishlist</a>
                                                    </p>
                                                </article>
                                            </div>
                                        </td>
                                        <td>Rs: 1,600</td>
                                        <td>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </td>
                                        <td>
                                            <p>Standard Delivery By </p>
                                            <p>06 Dec - 10 Dec</p>
                                        </td>
                                        <td>RS: 420</td>                                
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="cartfigsection">
                                                <figure class="cartfig float-left">
                                                    <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                </figure>
                                                <article>
                                                    <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                    <p>Orchid embroidery home furnishing articles... </p>
                                                    <p class="pt-2">
                                                        <a href="javascript:void(0)"><i class="fas fa-times"></i> Remove</a>
                                                        <a href="javascript:void(0)"><i class="far fa-heart"></i> Add to Wishlist</a>
                                                    </p>
                                                </article>
                                            </div>
                                        </td>
                                        <td>Rs: 1,600</td>
                                        <td>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </td>
                                        <td>
                                            <p>Standard Delivery By </p>
                                            <p>06 Dec - 10 Dec</p>
                                        </td>
                                        <td>RS: 420</td>                                
                                    </tr>                                    
                                </tbody>

                                <tfoot class="cartfoot">
                                    <tr>                                       
                                       
                                        <td>&nbsp;</td>
                                        <td>
                                            <p>Subtotal:</p>
                                            <p>Delivery Charges:</p>
                                        </td>
                                        <td>
                                            <p>Rs: 898 </p>
                                            <p>+Rs: 58</p>
                                        </td>
                                        <td colspan="2" align="right">
                                            <a href="checkout-deliveryaddress.php">Proceed to Pay Rs: 956</a>
                                        </td>
                                    </tr>
                                </tfoot>                                  
                            </table>    
                        </div>
                        <!--/ cart items -->
                    </div> 
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>