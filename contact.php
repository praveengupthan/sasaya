<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Contact Sasaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Contact </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>                                                          
                                <li class="nav-item"><a class="nav-link">Contact</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->

            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="colcontact rounded h-100 border-0 text-center">
                                <figure>
                                    <img src="img/support.svg" alt="" title="" class="svgimg py-5">
                                    <h4 class="text-uppercase">Call us at</h4>
                                    <p class="py-4">+91 9999999999</p>
                                </figure>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="colcontact rounded h-100 border-0 text-center">
                                <figure>
                                    <img src="img/placeholder.svg" alt="" title="" class="svgimg py-5">
                                    <h4 class="text-uppercase">Office Address</h4>
                                    <p class="py-4">PO Box 54378, 321 Your Address, Your City, Your Country</p>
                                </figure>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="colcontact rounded h-100 border-0 text-center">
                                <figure>
                                    <img src="img/mailbox.svg" alt="" title="" class="svgimg py-5">
                                    <h4 class="text-uppercase">Mail us at</h4>
                                    <p class="py-4">info@sasaya.com</p>
                                </figure>
                            </div>
                        </div>
                    </div>

                    <!-- row -->
                    <div class="formcol mt-4">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <h4 class="py-3">Drop us Message</h4>
                            </div>
                        </div>
                        <form class="form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">                                       
                                        <input class="form-control" type="text" placeholder="Enter Your Name">
                                    </div>
                                    <div class="form-group">                                        
                                        <input class="form-control" type="text" placeholder="Email Address">
                                    </div>
                                    <div class="form-group">                                       
                                        <input class="form-control" type="text" placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea  class="form-control">                                                
                                        </textarea>
                                    </div>
                                </div>
                            </div>                           
                        </form>
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <input type="submit" value="Send Message">
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>