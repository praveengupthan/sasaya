<header class="fixed-top">
    <!--news update -->
    <div class="newsupdates py-2 position-relative">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <a href="javascript:void(0)" class="text-uppercase">Buy more then <span>1000 select</span> free gift</a>
                </div>
            </div>
        </div>
        <a href="javascript:void(0)"><span class="closenews position-absolute"><i class="fas fa-times"></i></span></a>
    </div>
    <!--/ news update -->
    <!--top header -->
    <div class="topheader pt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link pl-0 pr-0">Welcome to Sayas Online Store  </a></li>
                        <li class="nav-item hidexs"><a class="nav-link pl-0 pr-0"><i class="far fa-clock"></i> MON - FRI: 10:00AM-18:00PM  </a></li>
                        <li class="nav-item hidexs"><a class="nav-link pl-0 pr-0"><i class="fas fa-phone-volume"></i> +7 849 55 4267  </a></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <nav class="navbar navbar-expand-md pt-0"> 
                        <!-- Toggler/collapsibe Button -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <i class="fas fa-bars"></i>
                        </button>                          
                        <!-- Navbar links -->
                        <div class="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="blog.php">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.php">Contact</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                                    <i class="far fa-user"></i> Account <i class="fas fa-caret-down"></i> 
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="login.php">Login</a>
                                    <a class="dropdown-item" href="signup.php">Register</a>
                                    <a class="dropdown-item" href="account-profileinfo.php">Your Account</a>
                                    <a class="dropdown-item" href="account-myorders.php">Your Orders</a>
                                </div>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="cart.php"><span></span> Cart <span>(2)</span></a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="mywishlist.php"><i class="far fa-heart"></i> Wish List</a>
                                </li>
                            </ul>
                        </div> 
                        </nav>
                </div>
            </div>
        </div>
    </div>
    <!--/ top header -->
    <!-- nav bar -->
    <nav id="topNav" class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="sasaya" title="Sasaya Online e Commerce Store" ></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbardropmain">
            Menu
        </button>
        <div id="navbardropmain" class="navbar-collapse collapse">
            <ul class="navbar-nav">
                <li class="nav-item navactive">
                    <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                            Gift Articles <i class="fas fa-caret-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>                        
                    </div>
                </li>   
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                            Aromas <i class="fas fa-caret-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>                        
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                            Tea's <i class="fas fa-caret-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>                        
                    </div>
                </li>  
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                            Accessories <i class="fas fa-caret-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>                        
                    </div>
                </li> 
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:void(0)" id="navbardrop" data-toggle="dropdown">
                            Salt <i class="fas fa-caret-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>
                        <a class="dropdown-item" href="productlist.php">Gift Sub Category</a>                        
                    </div>
                </li>  
                            
            </ul>
            <div class="ml-auto">
                <!-- search -->
                <div class="search float-left pt-1">
                    <input type="text" placeholder="Search here">
                    <input type="submit" value="Submit">
                </div>
                <!--/ search -->
                <ul class="navbar-nav headersocial">
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)"><i class="fab fa-twitter"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--/nav bar -->
</header>