<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>My Wish List</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Product Wishlist</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Wishlist</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">My Wishlist</h5>
                                <!-- account right body -->
                                <div class="rightprofile ">
                                    <div class="ordertab">
                                <div class="s-product  py-3">                                 
                                    
                                    <div class="row py-4 ordrow border-bottom">
                                        <div class="col-lg-2">
                                            <figure class="cartimg">
                                                <a href="account-myordersdetail.php"><img src="img/data/acc03.png"> </a>
                                            </figure>
                                        </div>
                                        <div class="col-lg-6 align-self-center">
                                            <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                            <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                            <div class="paybtns pt-3"> <a href="javascript:void(0)" class="cbtn btn text-uppercase fgray">Add to Cart</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                        </div>
                                        <div class="col-lg-4 align-self-right text-center">
                                            <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                        </div>
                                    </div>

                                    <div class="row py-4 ordrow border-bottom">
                                        <div class="col-lg-2">
                                            <figure class="cartimg">
                                                <a href="account-myordersdetail.php"><img src="img/data/acc03.png"> </a>
                                            </figure>
                                        </div>
                                        <div class="col-lg-6 align-self-center">
                                            <h5 class="fmed h6">AZC03D Intelligent Battery Digicharger Kit </h5>
                                            <p class="fgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                            <div class="paybtns pt-3"> <a href="javascript:void(0)" class="cbtn btn text-uppercase fgray">Add to Cart</a> <a href="account-myordersdetail.php" class="cbtn btn text-uppercase fgray">Order Details</a></div>
                                        </div>
                                        <div class="col-lg-4 align-self-right text-center">
                                            <h1 class="price h1"><i class="fas fa-rupee-sign"></i> 498</h1>
                                        </div>
                                    </div>


                                </div> 
                                </div>
                            </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->   
    <!--modal for add address -->
    <!-- The Modal -->
    <div class="modal" id="addaddress">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add a New Delivery Address</h4>
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <!-- address form -->
                    <form class="form addform">
                        <div class="form-group">
                            <input type="text" placeholder="Name" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Address" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Landmark" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="City" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="State" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Email" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="checkbox">
                            <label>Default Address</label>
                        </div>
                    </form>
                    <!--/ address form -->
                </div>
                <!-- Modal footer -->
                <div class="modal-footer justify-content-center pt-0 border-0">
                    <button type="button" class="btn text-uppercase cbtn">Confirm</button>
                    <button type="button" class="btn text-uppercase cbtn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal for add address --> 
</body>
</html>