<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Account profile infomration</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Account Profile </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Account Information</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">Profile Information</h5>
                                <!-- account right body -->
                                <div class="rightprofile">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <figure class="propic"> <img src="img/dummypic.jpg" alt="" title="">
                                                <!-- file  input-->
                                                <form action="#">
                                                    <div class="input-file-container">
                                                        <table class="inputfile">
                                                            <tr>
                                                                <td>
                                                                    <input class="input-file" id="my-file" type="file">
                                                                    <label tabindex="0" for="my-file" class="input-file-trigger"><i class="fas fa-camera"></i> Upload Image</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <p class="file-return"></p>
                                                </form>
                                                <!--/ file input -->
                                            </figure>
                                        </div>
                                        <div class="col-lg-8 rightprofile">
                                            <form class="py-4">
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Name</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="Praveen Kumar" class="form-control"> </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Your Gender</label>
                                                    </div>
                                                    <div class="col-lg-8 align-self-center">
                                                        <div class="form-group">
                                                            <label class="pr-3 pt-0">
                                                                <input type="radio"><span class="px-2">Male</span></label>
                                                            <label class="pr-3 pt-0">
                                                                <input type="radio"><span class="px-2">Female</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Date of Birth</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group yeardt">
                                                            <input type="text" placeholder="DD" class="form-control">
                                                            <input type="text" placeholder="MM" class="form-control">
                                                            <input type="text" placeholder="YYYY" class="form-control"> </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Phone Number</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="9849367037" class="form-control"> </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right pr-0">
                                                        <label>Email Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="santosh_n@yahoo.com" class="form-control"> </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 text-right">
                                                        <label>&nbsp;</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group mt-2"> 
                                                            <input type="submit" class="btn" value="Edit Profile">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>