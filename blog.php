<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Blog</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerblog.php'?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage pt-0">
               <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="blogcol  border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog01.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the drive</a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="blogcol border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog02.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the </a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="blogcol  border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog01.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the drive </a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="blogcol  border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog03.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the drive </a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="blogcol  border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog01.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the drive </a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="blogcol  border-bottom pb-3">
                                <figure class="pt-2"><a href="blogdetail.php"><img src="img/data/blog04.jpg" alt="" title="" class="img-fluid"></a></figure>
                                <h4 class="pb-2"><a href="blogdetail.php">Sasaya has given me the drive </a></h4>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. </p>
                                <ul class="bloginfo nav">
                                    <li class="nav-item">Posted on 01-02-2018  </li>
                                    <li class="nav-item">Posted by Admin</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page --> 

    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->    
</body>
</html>