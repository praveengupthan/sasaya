<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Checkout</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Checkout <span>3 (Products) </span> </h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="cart.php">Cart</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Review Order</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row checkout">               
                        <!-- checkout left -->
                        <div class="col-lg-9">                                            
                             <h5 class="h5 p-3 m-0 checkoutheading">Review Order</h5>
                                <div class="resp-tabs-container hor_1">                                   
                                    <!-- Review Order-->
                                    <div class="reveworder">
                                    <table class="table tableresp">
                                        <thead>
                                            <tr>
                                                <th>Item Details</th>
                                                <th align="center">Qty</th>
                                                <th>Delivery Details</th>                                                
                                                <th>Sub Total</th>                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="cartfigsection">
                                                        <figure class="cartfig float-left">
                                                            <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                        </figure>
                                                        <article>
                                                            <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                            <p><small>Orchid embroidery home furnishing articles... </small></p>
                                                            
                                                        </article>
                                                    </div>
                                                </td>
                                                <td align="center">
                                                    Rs: 249 x 1
                                                </td>
                                                <td>
                                                    <p>Standard Delivery By 06 Dec - 10 Dec </p>                                                    
                                                </td> 
                                                <td>
                                                    <table class="subtable">
                                                        <tr>
                                                            <td>Price</td>
                                                            <td>Rs: 399</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Delivery</td>
                                                            <td>+Rs: 50</td>
                                                        </tr>
                                                        <tr class="border-top">
                                                            <td>Total</td>
                                                            <td>Rs: 440</td>
                                                        </tr>
                                                    </table>
                                                </td>                                
                                            </tr>                                            
                                            <tr>
                                                <td>
                                                    <div class="cartfigsection">
                                                        <figure class="cartfig float-left">
                                                            <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                        </figure>
                                                        <article>
                                                            <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                            <p><small>Orchid embroidery home furnishing articles... </small></p>
                                                            
                                                        </article>
                                                    </div>
                                                </td>
                                                <td align="center">
                                                    Rs: 249 x 1
                                                </td>
                                                <td>
                                                    <p>Standard Delivery By 06 Dec - 10 Dec </p>                                                    
                                                </td> 
                                                <td>
                                                    <table class="subtable">
                                                        <tr>
                                                            <td>Price</td>
                                                            <td>Rs: 399</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Delivery</td>
                                                            <td>+Rs: 50</td>
                                                        </tr>
                                                        <tr class="border-top">
                                                            <td>Total</td>
                                                            <td>Rs: 440</td>
                                                        </tr>
                                                    </table>
                                                </td>                                
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <div class="cartfigsection">
                                                        <figure class="cartfig float-left">
                                                            <a href="javascript:void(0)"><img src="img/data/teas02.png" class="img-fluid" alt="" title=""></a>
                                                        </figure>
                                                        <article>
                                                            <h4 class="pb-1">Gift Article Item Name will be here </h4>  
                                                            <p><small>Orchid embroidery home furnishing articles... </small></p>                                                            
                                                        </article>
                                                    </div>
                                                </td>
                                                <td align="center">
                                                    Rs: 249 x 1
                                                </td>
                                                <td>
                                                    <p>Standard Delivery By 06 Dec - 10 Dec </p>                                                    
                                                </td> 
                                                <td>
                                                    <table class="subtable">
                                                        <tr>
                                                            <td>Price</td>
                                                            <td>Rs: 399</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Delivery</td>
                                                            <td>+Rs: 50</td>
                                                        </tr>
                                                        <tr class="border-top">
                                                            <td>Total</td>
                                                            <td>Rs: 440</td>
                                                        </tr>
                                                    </table>
                                                </td>                                
                                            </tr>                  
                                        </tbody>
                                        <tfoot class="cartfoot">
                                            <tr> 
                                                <td colspan="2">
                                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#mygift">Slect Free Gift</a>
                                                </td>    
                                                <td align="right">
                                                    <a class="text-uppercase" href="checkout-deliveryaddress.php">Back to Delivery Address</a>
                                                </td>
                                                <td>
                                                    <a href="makepayment.php">PAY NOW Rs: 956</a>
                                                </td>
                                            </tr>
                                        </tfoot>                                  
                                    </table>
                                    </div>
                                    <!--/Review Order-->                                   
                                </div>                 
                            </div>
                        <!--/ checkout left -->
                        <!-- Order summary -->
                        <div class="col-lg-3">
                            <?php include 'checkoutsummary.php' ?>                            
                        </div>
                        <!--/ order summary -->
                    </div> 
                </div>
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->    
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->  
  
<!-- free gifts starts  -->
<div id="mygift" class="modal fade modalcustom" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ">Select Your Free Gifts</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>       
      </div>
      <div class="modal-body">
         <!-- free gifts -->
         <div class="free-gifts">
            <h5 class="pb-1">Select Following Free Gift above Purchase of Rs: 700</h5>
            <p class="pb-2">Select any one following Gifts, This gift will deliver along with your Product </p>
            <table class="table tableresp">
                <thead>
                    <tr>
                        <th>Select</th>                                               
                        <th>Gift Image</th>                                                
                        <th>Name</th>                               
                        <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="radio" name="selgift"></td>
                            <td><img src="img/gift01.jpg" style="width:75px;"></td>
                            <td>Gold Collection of Chocolate </td>
                            <td>Package Containing 250 Grams, Black Chocklates Worth Rs: 350.00 </td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="selgift"></td>
                            <td><img src="img/gift01.jpg" style="width:75px;"></td>
                            <td>Gold Collection of Chocolate </td>
                            <td>Package Containing 250 Grams, Black Chocklates Worth Rs: 350.00 </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Confirm Gifts</button>
      </div>
    </div> 
  </div>
</div> 
<!-- free gifts ends -->

<!-- onload gifts -->
<div id="giftmodal" class="modal fade modalcustom" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ">Free Gifts on Every Purchase</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>       
        </div>
        <div class="modal-body">
            <img src="img/freegiftimg.jpg" class="img-fluid" alt="" title="">

            <!-- table for gift worth-->
            <table class="table tableresp">
                <thead>
                    <tr>
                        <th>Shopping Value</th>
                        <th>Gift Worth</th>                        
                        <th>View</th>                      
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <strong>Rs: 750 and Above </strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                        </td>                       
                        <td>Rs: 250</td>                        
                        <td><a href="javascript:void(0)">View Gifts</a></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Rs: 1200 and Above</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                         </td>
                        <td>Rs: 450</td>                        
                        <td><a href="javascript:void(0)">View Gifts</a></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Rs: 2200 and Above</strong> 
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                        </td>
                        <td>Rs: 650</td>                       
                        <td><a href="javascript:void(0)">View Gifts</a></td>
                    </tr>
                </tbody>
            </table>
            <!--/ table for gift worth -->            
         </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
        </div>
    </div> 
  </div>
</div> 
<!--/ on load gifts -->

<script type="text/javascript">
    $(window).on('load',function(){
        $('#giftmodal').modal('show');
    });
</script>

</body>
</html>