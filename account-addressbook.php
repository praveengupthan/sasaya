<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />    
    <title>Account Address Book</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/header -->
    <!--main -->
    <main>
        <!-- sub page-->
        <section class="subpage">
            <!-- subpage header -->
            <div class="pageheader position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <article>
                                <h2 class="">Address Book</h2>
                            </article>
                            <ul class="nav">
                                <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="productlist.php">User Name will be here</a></li>                                                              
                                <li class="nav-item"><a class="nav-link">Address Book</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
            <!--/ sub page header -->
            <!-- sub page body -->
            <div class="subpagebody">
                <div class="container">                  
                    <div class="row rowaccount">
                        <!-- left account nav-->
                        <div class="col-lg-3 border-right px-0">
                            <div class="cartheadrow">
                                <h5 class="h5 fmedf p-2">MY ACCOUNT</h5>
                            </div>
                            <?php include'accountprofile-nav.php' ?>
                        </div>
                        <!--/ left account nav -->
                        <!-- right account body -->
                        <div class="col-lg-9">
                            <div class="accountrt p-3">
                                <h5 class="h5 fmed border-bottom pb-3">My Address Book</h5>
                                <!-- account right body -->
                                <div class="rightprofile">
                                    <div class="row pt-3">
                                        <div class="col-lg-6 text-center ">
                                            <div class="addbox dashed rounded  addplus d-flex  align-items-center"> <a class="icadd mx-auto" href="javascript:void(0)" data-toggle="modal" data-target="#addaddress"><span class="acaddin"><i class="fas fa-plus"></i></span>
                                                    <span class="d-block">Add Address</span>
                                                </a> </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="addbox position-relative border rounded  align-items-center">
                                                <h4 class="h4">Praveen Kumar</h4>
                                                <h5 class="pb-2">9849367037</h5>
                                                <p class="fgray">2-3-64/10/A/23, Jaiswal Garden, Amberpet. Little Flower High School 500013 HYDERABAD TELANGANA</p>
                                                <p class="pt-3 editdel"> <a href="javascript:void(0)" class="text-uppercase pr-3"><i class="far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="text-uppercase"><i class="far fa-trash-alt"></i> Delete</a> </p> <span class="fred position-absolute Defaultadd text-uppercase">Default</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="addbox position-relative border rounded  align-items-center">
                                                <h4 class="h4">Praveen Kumar</h4>
                                                <h5 class="pb-2">9849367037</h5>
                                                <p class="fgray">2-3-64/10/A/23, Jaiswal Garden, Amberpet. Little Flower High School 500013 HYDERABAD TELANGANA</p>
                                                <p class="pt-3 editdel"> <a href="javascript:void(0)" class="text-uppercase pr-3"><i class="far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="text-uppercase pr-3"><i class="far fa-trash-alt"></i> Delete</a> </p> <span class="position-absolute Defaultadd text-uppercase">Default</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="addbox position-relative border rounded  align-items-center">
                                                <h4 class="h4">Raja Gonda</h4>
                                                <h5 class="pb-2">9849367037</h5>
                                                <p class="fgray">2-3-64/10/A/23, Jaiswal Garden, Amberpet. Little Flower High School 500013 HYDERABAD TELANGANA</p>
                                                <p class="pt-3 editdel"> <a href="javascript:void(0)" class="text-uppercase pr-3"><i class="far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="text-uppercase pr-3"><i class="far fa-trash-alt"></i> Delete</a> </p> <span class="position-absolute Defaultadd text-uppercase">Default</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ account right body -->
                            </div>
                        </div>
                        <!--/ right account body -->
                    </div>      
                </div>               
            </div>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main-->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <?php include 'footerscripts.php' ?>
    <!--/ footer -->   
    <!--modal for add address -->
    <!-- The Modal -->
    <div class="modal" id="addaddress">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add a New Delivery Address</h4>
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <!-- address form -->
                    <form class="form addform">
                        <div class="form-group">
                            <input type="text" placeholder="Name" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Address" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Landmark" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="City" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="State" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Email" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="text" placeholder="Pincode" class="form-control" name=""> </div>
                        <div class="form-group">
                            <input type="checkbox">
                            <label>Default Address</label>
                        </div>
                    </form>
                    <!--/ address form -->
                </div>
                <!-- Modal footer -->
                <div class="modal-footer justify-content-center pt-0 border-0">
                    <button type="button" class="btn text-uppercase cbtn">Confirm</button>
                    <button type="button" class="btn text-uppercase cbtn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal for add address --> 
</body>
</html>